Scriptable AdsManager for

- Applovin MAX SDK implementation
- IronSource sdk implementation

Has dependencies:

https://gitlab.com/p1284/gamereadypack.git

License
MIT License

Copyright © 2023 Gameready
