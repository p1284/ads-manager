using GameReady.Ads.Data;
using GameReady.EditorTools;
using UnityEditor;

namespace GameReady.Ads.Editor
{
    public static class AdsSetupHelper
    {
        private const string MAX_SDK = "MAX_SDK";
        private const string IRONSOURCE_SDK = "IRON_SOURCE";

        // [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/Create AdsManager", false, 0)]
        // public static void CreateAdsManager()
        // {
        //     EditorHelpers.CreateSceneInstance<AdsManager>("AdsManager");
        // }

#if CHEATS
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/Create Ads Cheats", false, 0)]
        public static void CreateAdsCheats()
        {
            EditorHelpers.CreateScriptableAsset<Gameready.Ads.Cheats.AdsCheats>(true, true);
        }
#endif

#if MAX_SDK
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/GetOrCreate MaxSDK Mediation", false, 1)]
        public static void CreateAdsMaxSDK()
        {
            EditorHelpers.CreateScriptableAsset<GameReady.Ads.MAXSDK.MaxSDKConfig>(true, true);
        }

#elif IRON_SOURCE
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/GetOrCreate IronSource Mediation", false, 1)]
        public static void CreateIronSourceMediation()
        {
            EditorHelpers.CreateScriptableAsset<GameReady.Ads.LevelPlay.IronSourceAdsConfig>(true, true);
        }
#endif

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/GetOrCreate MockAds Config", false, 1)]
        public static void CreateMockMediation()
        {
            EditorHelpers.CreateScriptableAsset<MockAdsManagerConfig>(true, true);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/Enable MAX_SDK", false, 2)]
        public static void EnableMaxSDK()
        {
            EditorHelpers.RemoveDefineForCurrentPlatform(IRONSOURCE_SDK);
            EditorHelpers.AddDefineForCurrentPlatform(MAX_SDK);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/Enable MAX_SDK", true, 2)]
        public static bool EnableMaxSDKValidate()
        {
            return !EditorHelpers.HasDefine(MAX_SDK);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/Enable IRON_SOURCE", false, 3)]
        public static void EnableIronSource()
        {
            EditorHelpers.RemoveDefineForCurrentPlatform(MAX_SDK);
            EditorHelpers.AddDefineForCurrentPlatform(IRONSOURCE_SDK);
        }

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Ads/Enable IRON_SOURCE", true, 3)]
        public static bool EnableIronSourceValidate()
        {
            return !EditorHelpers.HasDefine(IRONSOURCE_SDK);
        }
    }
}