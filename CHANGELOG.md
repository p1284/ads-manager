# Changelog

## [2.0.0] - 2023-10-09
### new implementation for Gameready package

## [1.0.0] - 2023-09-14
### First Release pack
### Implement MaxSDK
### Implement IronSource SDK