﻿using System;
using UnityEngine;

namespace GameReady.Ads.Data
{
    [Serializable]
    public class AdPlatformIds
    {
        public string InterstitialId;
        public string RewardId;
        public string BannerId;
    }

    [Serializable]
    public class AdIdsCollection
    {
        [SerializeField] private AdPlatformIds _android;
        [SerializeField] private AdPlatformIds _ios;

        public string BannerId
        {
#if UNITY_ANDROID
            get { return _android.BannerId; }


#elif UNITY_IOS
            get
            {
                return _ios.BannerId;
            }
#else
            get
            {
                return "";
            }
#endif
        }

        public string RewardId
        {
#if UNITY_ANDROID
            get { return _android.RewardId; }


#elif UNITY_IOS
            get
            {
                return _ios.RewardId;
            }
#else
            get
            {
                return "";
            }
#endif
        }

        public string InterstitialId
        {
#if UNITY_ANDROID
            get { return _android.InterstitialId; }


#elif UNITY_IOS
            get
            {
                return _ios.InterstitialId;
            }
#else
            get
            {
                return "";
            }
#endif
        }
        
        public string GetAdUnitId(AdsType adsType)
        {
            switch (adsType)
            {
                case AdsType.Interstitial:
                    return InterstitialId;
                case AdsType.Rewarded:
                    return RewardId;
                case AdsType.Banner:
                    return BannerId;
                default:
                    throw new ArgumentOutOfRangeException(nameof(adsType), adsType, null);
            }
        }
    }
}