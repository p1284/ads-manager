using GameReady.Ads.Core;
using GameReady.Analytics.Data;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;

namespace GameReady.Ads.Data
{
    
    public abstract class BaseAdsManagerConfig : BaseServiceConfig
    {
        [Tooltip("Не показывать interstitial рекламу после rewarded в течении X секунд.")] [SerializeField]
        internal float _ignoreInterstitialAfterRewarded = 60;

        [Tooltip("Не показывать interstitial рекламу после interstitial в течении X секунд.")] [SerializeField]
        internal float _ignoreInterstitialAfterInterstitial = 60;

        [Tooltip("Блокировать inter после инита на IgnoreInterstitialAfterInterstitial")] [SerializeField]
        internal bool _startInterstitialAfterInterstitialTimerOnInit;

        [Header("ANALYTICS")] [SerializeField] internal bool _sendPaidRevenueAnalytics;
        [SerializeField] internal AnalyticEventKey _adsImpressionKey;
    }
}