using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.Ads.Data
{
    public class MockAdsManagerConfig : BaseAdsManagerConfig
    {
        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<MockAdsMediation>().As<IAdsManager>().WithParameter(this);
        }
    }
}