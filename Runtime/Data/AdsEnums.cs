﻿namespace GameReady.Ads.Data
{
    public enum AdsType
    {
        Interstitial = 0,
        Rewarded = 1,
        Banner = 2
    }

    public enum AdsShowResult
    {
        ShowComplete,
        ShowFailed,
        Clicked,
        NotReady,
        Closed
    }

    internal enum AdsShowState
    {
        ReadyForShowAds = 0,
        StartShowInterstitial,
        StartShowRewarded
    }
}