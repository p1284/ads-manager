#if IRON_SOURCE
using System;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using Gameready.Utils;
using UnityEngine;

namespace GameReady.Ads.LevelPlay
{
    public class RewardAds : IAdsImplementation
    {
        private readonly IronSourceMediation _mediation;

        private Action<AdsShowResult> _callback;
        private string _placement;

        public RewardAds(IronSourceMediation mediation)
        {
            _mediation = mediation;
        }

        public void Dispose()
        {
            //  IronSourceRewardedVideoEvents.onAdOpenedEvent -= RewardedVideoOnAdOpenedEvent;
            IronSourceRewardedVideoEvents.onAdClosedEvent -= RewardedVideoOnAdClosedEvent;
            // IronSourceRewardedVideoEvents.onAdAvailableEvent -= RewardedVideoOnAdAvailable;
            IronSourceRewardedVideoEvents.onAdUnavailableEvent -= RewardedVideoOnAdUnavailable;
            IronSourceRewardedVideoEvents.onAdShowFailedEvent -= RewardedVideoOnAdShowFailedEvent;
            IronSourceRewardedVideoEvents.onAdRewardedEvent -= RewardedVideoOnAdRewardedEvent;
            IronSourceRewardedVideoEvents.onAdClickedEvent -= RewardedVideoOnAdClickedEvent;
            IsInitialized = false;
        }

        public void Initialize()
        {
            //  IronSourceRewardedVideoEvents.onAdOpenedEvent += RewardedVideoOnAdOpenedEvent;
            IronSourceRewardedVideoEvents.onAdClosedEvent += RewardedVideoOnAdClosedEvent;
            // IronSourceRewardedVideoEvents.onAdAvailableEvent += RewardedVideoOnAdAvailable;
            IronSourceRewardedVideoEvents.onAdUnavailableEvent += RewardedVideoOnAdUnavailable;
            IronSourceRewardedVideoEvents.onAdShowFailedEvent += RewardedVideoOnAdShowFailedEvent;
            IronSourceRewardedVideoEvents.onAdRewardedEvent += RewardedVideoOnAdRewardedEvent;
            IronSourceRewardedVideoEvents.onAdClickedEvent += RewardedVideoOnAdClickedEvent;

            LoadRewardedAdAsync().Forget();
            IsInitialized = true;
        }

        private async UniTaskVoid LoadRewardedAdAsync(float delay = 0f)
        {
            if (delay >= 0)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(delay));
            }

            IronSource.Agent.loadRewardedVideo();
        }

        private void RewardedVideoOnAdClickedEvent(IronSourcePlacement arg1, IronSourceAdInfo arg2)
        {
            _callback?.Invoke(AdsShowResult.Clicked);
            _mediation.AdClicked(AdsType.Rewarded);
        }

        private void RewardedVideoOnAdRewardedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo)
        {
            _callback?.Invoke(AdsShowResult.ShowComplete);

            _mediation.AdShowed(AdsType.Rewarded, true);

            _mediation._interstitialBlockedAfterRewarded = true;

            _mediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            _mediation.BlockInterstitialAfterRewards().Forget();
        }

        private void RewardedVideoOnAdShowFailedEvent(IronSourceError errorInfo, IronSourceAdInfo adInfo)
        {
            _callback?.Invoke(AdsShowResult.ShowFailed);
            _mediation.AdShowed(AdsType.Rewarded, false);
            _mediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            _mediation.Log(
                $"error: {errorInfo.getDescription()} code:{errorInfo.getErrorCode()}",
                true);
            // Rewarded ad failed to display. We recommend loading the next ad
            LoadRewardedAdAsync().Forget();
        }

        private void RewardedVideoOnAdUnavailable()
        {
            // Rewarded ad failed to load. We recommend retrying with exponentially higher delays.
            _mediation.Log($"rewarded ads unavailable", true);
            _mediation._rewardedLoadRetryAttempt++;
            float retryDelay = Mathf.Pow(2, _mediation._rewardedLoadRetryAttempt);

            LoadRewardedAdAsync(retryDelay).Forget();

            _mediation.AdLoaded(AdsType.Rewarded, false);
        }


        private void RewardedVideoOnAdClosedEvent(IronSourceAdInfo obj)
        {
            _callback?.Invoke(AdsShowResult.Closed);
            // Interstitial ad is hidden. Pre-load the next ad
            _mediation.AdClosed(AdsType.Rewarded);

            // Rewarded ad is hidden. Pre-load the next ad
            LoadRewardedAdAsync().Forget();
            _mediation.CurrentShowState = AdsShowState.ReadyForShowAds;
        }


        public bool IsInitialized { get; private set; }

        public void ShowAds(Action<AdsShowResult> callback, string placement = "")
        {
            _callback = callback;
            _placement = placement;

            if (IronSource.Agent.isRewardedVideoAvailable())
            {
                IronSource.Agent.showRewardedVideo(_placement);

                NetworkCheck.CheckConnection();

                _mediation.CurrentShowState = AdsShowState.StartShowRewarded;
            }
            else
            {
                _callback?.Invoke(AdsShowResult.NotReady);
                _mediation.AdShowed(AdsType.Rewarded, false);
            }
        }

        public void Preload()
        {
            IronSource.Agent.loadRewardedVideo();
        }

        public bool IsLoaded() => IsInitialized && IronSource.Agent.isRewardedVideoAvailable();
    }
}
#endif