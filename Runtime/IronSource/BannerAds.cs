#if IRON_SOURCE
using System;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;

namespace GameReady.Ads.LevelPlay
{
    public class BannerAds : IAdsImplementation
    {
        private readonly IronSourceMediation _mediation;

        private Action<AdsShowResult> _callback;
        private string _placement;
        private bool _isActivated;

        public BannerAds(IronSourceMediation mediation)
        {
            _mediation = mediation;
        }

        public void Dispose()
        {
            if (!IsInitialized) return;
            IronSourceBannerEvents.onAdLoadedEvent -= OnBannerLoadedHandler;
            IronSourceBannerEvents.onAdLoadFailedEvent -= OnBannerLoadFailedHandler;
            IronSourceBannerEvents.onAdClickedEvent -= OnAdClickedHandler;

            IronSource.Agent.destroyBanner();
            IsInitialized = false;
        }

        public void Initialize()
        {
            if (!_mediation._config._useBanner) return;

            IronSourceBannerEvents.onAdLoadedEvent += OnBannerLoadedHandler;
            IronSourceBannerEvents.onAdLoadFailedEvent += OnBannerLoadFailedHandler;
            IronSourceBannerEvents.onAdClickedEvent += OnAdClickedHandler;

            Preload();
        }


        private void OnAdClickedHandler(IronSourceAdInfo adInfo)
        {
            _callback?.Invoke(AdsShowResult.Clicked);
            _mediation.AdClicked(AdsType.Banner);
        }

        private void OnBannerLoadedHandler(IronSourceAdInfo adInfo)
        {
            IsInitialized = true;
            _mediation.AdLoaded(AdsType.Banner, true);
            _callback?.Invoke(AdsShowResult.ShowComplete);

            if (_mediation._config._showBannerAfterInit)
            {
                ShowAds(null);
            }
        }

        private void OnBannerLoadFailedHandler(IronSourceError obj)
        {
            _isActivated = false;
            IsInitialized = false;

            _mediation.AdLoaded(AdsType.Banner, false);
            _mediation.Log($"banner not loaded:{obj.getDescription()}");
            _callback?.Invoke(AdsShowResult.ShowFailed);
        }


        public bool IsInitialized { get; private set; }

        public void ShowAds(Action<AdsShowResult> callback, string placement = "")
        {
            if (_isActivated)
            {
                _mediation.Log("banner activated already");
                return;
            }

            _callback = callback;
            _placement = placement;

            if (IsInitialized)
            {
                IronSource.Agent.displayBanner();
            }
            else
            {
                WaitForInitAsync().Forget();
            }
        }

        private async UniTaskVoid WaitForInitAsync()
        {
            await UniTask.WaitUntil(() => IsInitialized);
            IronSource.Agent.displayBanner();
        }

        public void Preload()
        {
            if (_isActivated) return;
            _isActivated = true;
            IronSourceBannerSize size = _mediation._config._bannerSizeDescription == IronSourceBannerSizeDescription.Custom
                ? new IronSourceBannerSize(_mediation._config._bannerSize.x, _mediation._config._bannerSize.y)
                : new IronSourceBannerSize(_mediation._config._bannerSizeDescription.ToString().ToUpper());

            IronSource.Agent.loadBanner(size, _mediation._config._bannerPosition);
        }

        public bool IsLoaded() => IsInitialized && _isActivated;
    }
}
#endif