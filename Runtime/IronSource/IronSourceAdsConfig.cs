using GameReady.Ads.Data;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.Ads.LevelPlay
{
    public class IronSourceAdsConfig : BaseAdsManagerConfig
    {
        [SerializeField] internal bool _showBannerAfterInit;
        [SerializeField] internal bool _useBanner;
        [SerializeField] internal Vector2Int _bannerSize;
#if IRON_SOURCE
        [SerializeField] internal IronSourceBannerSizeDescription _bannerSizeDescription;
        [SerializeField] internal IronSourceBannerPosition _bannerPosition;
#endif

        public override void RegisterService(IContainerBuilder builder)
        {
#if IRON_SOURCE
            builder.RegisterEntryPoint<IronSourceMediation>().As<IAdsManager>().WithParameter(this);
#endif
        }
    }
}