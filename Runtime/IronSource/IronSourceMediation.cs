#if IRON_SOURCE
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using GameReady.Analytics;
using GameReady.Analytics.Data;
using Gameready.Utils;
using UnityEngine;
using VContainer.Unity;

namespace GameReady.Ads.LevelPlay
{
    public enum IronSourceBannerSizeDescription
    {
        Large,
        Smart,
        Banner,
        Rectangle,
        Custom
    }

    /// <summary>
    /// TODO make it work
    /// </summary>
    public class IronSourceMediation : BaseAdsMediation, IPauseFocusChanged, IAsyncStartable
    {
        private IronSourceMediationSettings _settings;

        internal readonly IronSourceAdsConfig _config;
        private readonly IAnalyticsManager _analytics;

        public IronSourceMediation(IronSourceAdsConfig config, IAnalyticsManager analyticsManager) : base(config)
        {
            _config = config;
            _analytics = analyticsManager;
        }

        public async UniTask StartAsync(CancellationToken cancellation)
        {
            MonoHelper.AddPauseFocusListener(this);

            try
            {
                var settings =
                    await Resources.LoadAsync<IronSourceMediationSettings>(IronSourceMediationSettings
                        .IRONSOURCE_SETTINGS_ASSET_PATH);
                _settings = (IronSourceMediationSettings)settings;

                IsInitialized = _settings.EnableIronsourceSDKInitAPI;

                _adsImplementations = new ReadOnlyDictionary<AdsType, IAdsImplementation>(
                    new Dictionary<AdsType, IAdsImplementation>()
                    {
                        { AdsType.Banner, new BannerAds(this) },
                        { AdsType.Interstitial, new InterstitialAds(this) },
                        { AdsType.Rewarded, new RewardAds(this) }
                    });

                if (_config._sendPaidRevenueAnalytics)
                {
                    IronSourceEvents.onImpressionDataReadyEvent += ImpressionDataReadyEvent;
                }

                if (!IsInitialized)
                {
                    IronSourceEvents.onSdkInitializationCompletedEvent += SdkInitializationCompletedEvent;

                    Debug.Log("unity-script: IronSource.Agent.validateIntegration");
                    IronSource.Agent.validateIntegration();

                    Debug.Log("unity-script: unity version" + global::IronSource.unityVersion());

                    // SDK init
                    Debug.Log("unity-script: IronSource.Agent.init");
#if UNITY_ANDROID

                    IronSource.Agent.init(_settings.AndroidAppKey);

#elif UNITY_IOS
                IronSource.Agent.init(_settings.IOSAppKey);
#else
                IronSource.setUnsupportedPlatform();
                Debug.Log("Init Not Supported Platform");
#endif
                }
                else
                {
                    SdkInitializationCompletedEvent();
                }
            }
            catch (Exception e)
            {
                Log(e.Message, true);
            }
        }

        private void ImpressionDataReadyEvent(IronSourceImpressionData data)
        {
            if (data.revenue == null)
            {
                return;
            }

            if (_analytics == null) return;
            var tracker = _analytics.CreateTracker(_config._adsImpressionKey.EventName);

            if (tracker == null) return;

            var countryCode = data.country;

            var adImpressionData = new AdsImpressionAnalyticData(data.adNetwork,
                AdsShowResult.ShowComplete.ToString(), data.placement)
            {
                AdUnit = data.adUnit,
                NetworkName = data.adNetwork,
                Revenue = data.revenue.Value,
                CountryCode = countryCode,
                DspName = data.instanceName,
                AdProvider = data.segmentName
            };

            tracker.TrackAdsImpression(adImpressionData, _config._adsImpressionKey.Filter);

            Log("[GET REVENUE] revenue: " + adImpressionData.Revenue + " | countryCode: " + countryCode +
                " | networkName: " + adImpressionData.NetworkName + " | adUnitIdentifier: " + adImpressionData.AdUnit +
                " | placement: " +
                adImpressionData.Placement);
        }


        private async UniTaskVoid InitAsync()
        {
        }

        private void SdkInitializationCompletedEvent()
        {
            IronSourceEvents.onSdkInitializationCompletedEvent -= SdkInitializationCompletedEvent;

            //TODO: Check user consent

            foreach (var adsImplementation in _adsImplementations)
            {
                var adsType = adsImplementation.Key;
                if (!IsAdsRemoved(adsType))
                {
                    adsImplementation.Value.Initialize();
                }
            }

            IsInitialized = true;
            SdkInitialized();
            Log("IronSource Initialized");
        }


        public override void ShowAds(AdsType adsType, Action<AdsShowResult> callback = null, string placement = "")
        {
            if (PreCondition("null", adsType))
            {
                var adsImplementation = _adsImplementations[adsType];

                adsImplementation.ShowAds(result => { callback?.Invoke(result); });
            }
            else
            {
                callback?.Invoke(AdsShowResult.NotReady);
                AdShowed(adsType, false);
            }
        }

        void IPauseFocusChanged.OnPaused(bool paused)
        {
            IronSource.Agent.onApplicationPause(paused);
        }

        void IPauseFocusChanged.OnFocus(bool focused)
        {
        }

        public override void Dispose()
        {
            base.Dispose();
            if (_config._sendPaidRevenueAnalytics)
            {
                IronSourceEvents.onImpressionDataReadyEvent -= ImpressionDataReadyEvent;
            }
        }
    }
}
#endif