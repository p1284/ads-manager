#if IRON_SOURCE
using System;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using Gameready.Utils;
using UnityEngine;

namespace GameReady.Ads.LevelPlay
{
    public class InterstitialAds : IAdsImplementation
    {
        private readonly IronSourceMediation _mediation;

        private string _placement;

        private Action<AdsShowResult> _callback;

        public InterstitialAds(IronSourceMediation mediation)
        {
            _mediation = mediation;
        }


        public void Dispose()
        {
            IronSourceInterstitialEvents.onAdReadyEvent -= InterstitialOnAdReadyEvent;
            IronSourceInterstitialEvents.onAdLoadFailedEvent -= InterstitialOnAdLoadFailed;
            // IronSourceInterstitialEvents.onAdOpenedEvent -= InterstitialOnAdOpenedEvent;
            IronSourceInterstitialEvents.onAdClickedEvent -= InterstitialOnAdClickedEvent;
            IronSourceInterstitialEvents.onAdShowSucceededEvent -= InterstitialOnAdShowSucceededEvent;
            IronSourceInterstitialEvents.onAdShowFailedEvent -= InterstitialOnAdShowFailedEvent;
            IronSourceInterstitialEvents.onAdClosedEvent -= InterstitialOnAdClosedEvent;
            IsInitialized = false;
        }

        public void Initialize()
        {
            IronSourceInterstitialEvents.onAdReadyEvent += InterstitialOnAdReadyEvent;
            IronSourceInterstitialEvents.onAdLoadFailedEvent += InterstitialOnAdLoadFailed;
            //IronSourceInterstitialEvents.onAdOpenedEvent += InterstitialOnAdOpenedEvent;
            IronSourceInterstitialEvents.onAdClickedEvent += InterstitialOnAdClickedEvent;
            IronSourceInterstitialEvents.onAdShowSucceededEvent += InterstitialOnAdShowSucceededEvent;
            IronSourceInterstitialEvents.onAdShowFailedEvent += InterstitialOnAdShowFailedEvent;
            IronSourceInterstitialEvents.onAdClosedEvent += InterstitialOnAdClosedEvent;
         
            // Load the first interstitial
            LoadInterstitialAsync().Forget();


            if (_mediation._config._startInterstitialAfterInterstitialTimerOnInit)
            {
                // Starts blocking timer so we don't see first ad in less than timer value
                _mediation._interstitialBlockedAfterInterstitial = true;
                _mediation.BlockInterstitialAfterInterstitial().Forget();
            }

            IsInitialized = true;
        }

        private async UniTaskVoid LoadInterstitialAsync(float delay = 0)
        {
            if (delay >= 0)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(delay));
            }

            IronSource.Agent.loadInterstitial();
        }


        private void InterstitialOnAdClosedEvent(IronSourceAdInfo obj)
        {
            _callback?.Invoke(AdsShowResult.Closed);
            // Interstitial ad is hidden. Pre-load the next ad
            _mediation.AdClosed(AdsType.Interstitial);

            _mediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            LoadInterstitialAsync().Forget();
        }

        private void InterstitialOnAdShowFailedEvent(IronSourceError errorInfo, IronSourceAdInfo adInfo)
        {
            _mediation.Log("Interstitial failed to display with error code: " + errorInfo.getErrorCode(), true);

            _callback?.Invoke(AdsShowResult.ShowFailed);
            _mediation.AdShowed(AdsType.Interstitial, false);

            _mediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            LoadInterstitialAsync().Forget();
        }

        private void InterstitialOnAdShowSucceededEvent(IronSourceAdInfo adInfo)
        {
            _callback?.Invoke(AdsShowResult.ShowComplete);
            _mediation.AdShowed(AdsType.Interstitial, !Application.isEditor);
            _mediation._interstitialBlockedAfterInterstitial = true;
            _mediation.BlockInterstitialAfterInterstitial().Forget();
            
            
        }

        private void InterstitialOnAdClickedEvent(IronSourceAdInfo obj)
        {
            _callback?.Invoke(AdsShowResult.Clicked);
            _mediation.AdClicked(AdsType.Interstitial);
        }

        private void InterstitialOnAdLoadFailed(IronSourceError errorInfo)
        {
            // if (errorInfo.getErrorCode()  == MaxSdkBase.ErrorCode.NetworkError || errorInfo.Code == MaxSdkBase.ErrorCode.NoNetwork)
            // {
            //     NetworkCheck.CheckConnection();
            // }

            // Interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
            _mediation._interstitialRetryAttempt++;

            float retryDelay = Mathf.Pow(2, _mediation._interstitialRetryAttempt);

            _mediation._interstitialRetryAttempt = Mathf.Clamp(_mediation._interstitialRetryAttempt, 1, 6);

            _mediation.AdLoaded(AdsType.Interstitial, false);

            _mediation.Log("Interstitial failed to load with error code: " + errorInfo.getCode(), true);

            _mediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            LoadInterstitialAsync(retryDelay).Forget();
        }

        private void InterstitialOnAdReadyEvent(IronSourceAdInfo obj)
        {
            _mediation.Log($"Interstitial loaded {_placement}");

            // Reset retry attempt
            _mediation._interstitialRetryAttempt = 0;
            _mediation.AdLoaded(AdsType.Interstitial, true);
        }

        public bool IsInitialized { get; private set; }

        public void ShowAds(Action<AdsShowResult> callback, string placement = "")
        {
            _callback = callback;
            _placement = placement;

            if (IronSource.Agent.isInterstitialReady())
            {
                IronSource.Agent.showInterstitial(_placement);

                NetworkCheck.CheckConnection();
                _mediation.CurrentShowState = AdsShowState.StartShowInterstitial;
            }
            else
            {
                _callback?.Invoke(AdsShowResult.NotReady);
                _mediation.AdShowed(AdsType.Interstitial, false);
            }
        }

        public void Preload()
        {
            if (!IsInitialized) return;
            IronSource.Agent.loadInterstitial();
        }

        public bool IsLoaded() => IsInitialized && IronSource.Agent.isInterstitialReady();
    }
}
#endif