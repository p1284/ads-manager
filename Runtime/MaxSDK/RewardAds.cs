﻿#if MAX_SDK
using System;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using Gameready.Utils;
using UnityEngine;

namespace GameReady.Ads.MAXSDK
{
    public class RewardAds : IAdsImplementation
    {
        private readonly MaxSDKMediation _maxSDKMediation;

        private string _placement;
        private bool _firstLaunch;

        private bool _clicked;
        private Action<AdsShowResult> _callback;

        private readonly string _adUnitId;

       // private AnalyticsEventsTracker _analyticsEvents;

        public RewardAds(MaxSDKMediation maxSDKMediation)
        {
            _maxSDKMediation = maxSDKMediation;
            _adUnitId = _maxSDKMediation._config._adIdsCollection.RewardId;
        }

        public void Initialize()
        {
          //  _analyticsEvents = _maxSDKMediation._analyticsManager?.CreateTracker();
            MaxSdkCallbacks.Rewarded.OnAdClickedEvent += OnRewardedAdsClickHandler;
            MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent += OnRewardedAdFailedEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += OnRewardedAdHiddenEvent;
            MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;
            if (_maxSDKMediation._config._sendPaidRevenueAnalytics)
                MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent += OnAdRevenuePaidHandler;
            // Load the first RewardedAd
            LoadRewardedAdAsync().Forget();
            IsInitialized = true;
        }

        private void OnAdRevenuePaidHandler(string adUnit, MaxSdkBase.AdInfo adInfo)
        {
            _maxSDKMediation.OnAdRevenuePaidEvent(AdsType.Rewarded, adUnit, adInfo);
        }

        public bool IsInitialized { get; private set; }

        public void ShowAds(Action<AdsShowResult> callback, string placement = "")
        {
            _callback = callback;
            _placement = placement;

            if (MaxSdk.IsRewardedAdReady(_adUnitId))
            {
                _firstLaunch = true;
                _clicked = false;

                MaxSdk.ShowRewardedAd(_adUnitId, _placement);

                NetworkCheck.CheckConnection();

                _maxSDKMediation.CurrentShowState = AdsShowState.StartShowRewarded;


                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
                //     .SetParamValue("type", "reward")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "success")
                //     .SetParamValue("connection",
                //         ((int)NetworkCheck.LastConnectionStatus).ToString())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
                //
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsStarted)
                //     .SetParamValue("type", "reward")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "start")
                //     .SetParamValue("connection",
                //         ((int)NetworkCheck.LastConnectionStatus).ToString())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }
            else
            {
                if (_firstLaunch)
                {
                    // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
                    //     .SetParamValue("type", "reward")
                    //     .SetParamValue("placement", _placement)
                    //     .SetParamValue("result", "not_ready")
                    //     .SetParamValue("connection",
                    //         ((int)NetworkCheck.LastConnectionStatus).ToString())
                    //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
                }

                _callback?.Invoke(AdsShowResult.NotReady);
                _maxSDKMediation.AdShowed(AdsType.Rewarded, false);
            }
        }

        public void Preload()
        {
            MaxSdk.LoadRewardedAd(_adUnitId);
        }

        public bool IsLoaded()
        {
            return IsInitialized && MaxSdk.IsRewardedAdReady(_adUnitId);
        }

        public void Dispose()
        {
            IsInitialized = false;
            MaxSdkCallbacks.Rewarded.OnAdClickedEvent -= OnRewardedAdsClickHandler;
            MaxSdkCallbacks.Rewarded.OnAdLoadedEvent -= OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.Rewarded.OnAdLoadFailedEvent -= OnRewardedAdFailedEvent;
            MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent -= OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.Rewarded.OnAdHiddenEvent -= OnRewardedAdHiddenEvent;
            MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent -= OnRewardedAdReceivedRewardEvent;
            MaxSdkCallbacks.Rewarded.OnAdRevenuePaidEvent -= OnAdRevenuePaidHandler;
        }

        private void OnRewardedAdsClickHandler(string adUnit, MaxSdkBase.AdInfo arg2)
        {
            _clicked = true;
            _callback?.Invoke(AdsShowResult.Clicked);
            _maxSDKMediation.AdClicked(AdsType.Rewarded);
        }

        private async UniTaskVoid LoadRewardedAdAsync(float delay = 0f)
        {
            if (delay >= 0)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(delay));
            }

            MaxSdk.LoadRewardedAd(_adUnitId);
        }

        private void OnRewardedAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            NetworkCheck.CheckConnection();
            // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'

            // Reset retry attempt
            _maxSDKMediation._rewardedLoadRetryAttempt = 0;
            _maxSDKMediation.AdLoaded(AdsType.Rewarded, true);
        }

        private void OnRewardedAdFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            if (errorInfo.Code == MaxSdkBase.ErrorCode.NetworkError || errorInfo.Code == MaxSdkBase.ErrorCode.NoNetwork)
            {
                NetworkCheck.CheckConnection();
            }

            // Rewarded ad failed to load. We recommend retrying with exponentially higher delays.
            _maxSDKMediation.Log($"rewarded ads failed {errorInfo.Message}", true);
            _maxSDKMediation._rewardedLoadRetryAttempt++;
            float retryDelay = Mathf.Pow(2, _maxSDKMediation._rewardedLoadRetryAttempt);

            LoadRewardedAdAsync(retryDelay).Forget();

            _maxSDKMediation.AdLoaded(AdsType.Rewarded, false);
        }

        private void OnRewardedAdFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo,
            MaxSdkBase.AdInfo arg3)
        {
            if (errorInfo.Code == MaxSdkBase.ErrorCode.NetworkError || errorInfo.Code == MaxSdkBase.ErrorCode.NoNetwork)
            {
                NetworkCheck.CheckConnection();
            }

            if (MaxSdk.IsRewardedAdReady(_adUnitId) &&
                _maxSDKMediation.CurrentShowState != AdsShowState.StartShowRewarded)
            {
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
                //     .SetParamValue("type", "reward")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "cancel")
                //     .SetParamValue("connection",
                //         (NetworkCheck.LastConnectionStatus).ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }

            _callback?.Invoke(AdsShowResult.ShowFailed);
            _maxSDKMediation.AdShowed(AdsType.Rewarded, false);
            _maxSDKMediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            _maxSDKMediation.Log(
                $"error: {errorInfo.Message} code:{errorInfo.Code} adFailInfo:{errorInfo.AdLoadFailureInfo}",
                true);
            // Rewarded ad failed to display. We recommend loading the next ad
            LoadRewardedAdAsync().Forget();
        }

        private void OnRewardedAdHiddenEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            if (!_clicked)
            {
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsWatched)
                //     .SetParamValue("type", "reward")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "canceled").SetParamValue("connection",
                //         (NetworkCheck.LastConnectionStatus).ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }
            else
            {
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsWatched)
                //     .SetParamValue("type", "reward")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "clicked")
                //     .SetParamValue("connection",
                //         (NetworkCheck.LastConnectionStatus).ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }

            // if rewarded actions are not null at this point then ad was closed before finishing
            _callback?.Invoke(AdsShowResult.Closed);
            // Interstitial ad is hidden. Pre-load the next ad
            _maxSDKMediation.AdClosed(AdsType.Rewarded);

            // Rewarded ad is hidden. Pre-load the next ad
            LoadRewardedAdAsync().Forget();
            _maxSDKMediation.CurrentShowState = AdsShowState.ReadyForShowAds;
        }

        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward, MaxSdkBase.AdInfo arg3)
        {
            _callback?.Invoke(AdsShowResult.ShowComplete);

            _maxSDKMediation.AdShowed(AdsType.Rewarded, true);

            _maxSDKMediation._interstitialBlockedAfterRewarded = true;

            _maxSDKMediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            if (!_clicked)
            {
                _clicked = true;
            }

            _maxSDKMediation.BlockInterstitialAfterRewards().Forget();
        }

       
    }
}
#endif