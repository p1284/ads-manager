﻿#if MAX_SDK
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using GameReady.Analytics;
using GameReady.Analytics.Data;
using GameReady.UI.Popups;
using GameReady.UI.Popups.Data;
using UnityEngine;
using VContainer.Unity;

namespace GameReady.Ads.MAXSDK
{
    public class MaxSDKMediation : BaseAdsMediation, IMediationDebuggerSupport, IAsyncStartable
    {
        private const string PrivacyConsentPrefKey = "max_sdk_privacy_consent";

        internal readonly MaxSDKConfig _config;
        private readonly IAnalyticsManager _analytics;
        private readonly IPopupsManager _popupsManager;
        
        public MaxSDKMediation(MaxSDKConfig config, 
            IAnalyticsManager analyticsManager, 
            IPopupsManager popupsManager) :
            base(config)
        {
            _config = config;
            _analytics = analyticsManager;
            _popupsManager = popupsManager;
        }

        public async UniTask StartAsync(CancellationToken cancellation)
        {
            if (_analytics != null)
            {
                await UniTask.WaitUntil(() => _analytics == null || _analytics.IsInitialized, cancellationToken: cancellation);
            }

            _adsImplementations = new ReadOnlyDictionary<AdsType, IAdsImplementation>(
                new Dictionary<AdsType, IAdsImplementation>()
                {
                    {
                        AdsType.Interstitial,
                        new InterstitialAds(this)
                    },
                    {
                        AdsType.Rewarded,
                        new RewardAds(this)
                    },
                    {
                        AdsType.Banner,
                        new BannerAds(this)
                    }
                });

            MaxSdkCallbacks.OnSdkInitializedEvent += OnSDKInitializedHandler;

            MaxSdk.SetIsAgeRestrictedUser(_config._isAgeRestricted);

            MaxSdk.SetSdkKey(_config.MaxSDKKey);
            MaxSdk.InitializeSdk();

            if (_config.UsePrivacyConsent && !HasUserConsent())
            {
                await UniTask.WaitUntil(() => IsInitialized, cancellationToken: cancellation);
            }
            else
            {
                await UniTask.Yield();
            }
        }

        private bool HasUserConsent()
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(PrivacyConsentPrefKey, 0));
        }

        private void SetUserConsent(bool value)
        {
            MaxSdk.SetHasUserConsent(value);

            if (!IsInitialized)
            {
                Activate();
            }

            PlayerPrefs.SetInt(PrivacyConsentPrefKey, Convert.ToInt32(value));
        }

        private void OnSDKInitializedHandler(MaxSdkBase.SdkConfiguration sdkConfiguration)
        {
            MaxSdkCallbacks.OnSdkInitializedEvent -= OnSDKInitializedHandler;

            if (_config.UsePrivacyConsent && !HasUserConsent())
            {
                ProcessUserConsent(sdkConfiguration);
            }
            else
            {
                Activate();
            }

            // AppLovin SDK is initialized, configure and start loading ads.
            Log("MAX SDK Initialized");
            SdkInitialized();
        }


        private void Activate()
        {
            if (IsInitialized) return;

            foreach (var adsImplementation in _adsImplementations)
            {
                var adsType = adsImplementation.Key;
                if (string.IsNullOrEmpty(_config._adIdsCollection.GetAdUnitId(adsType)))
                {
                    Log($"{adsType} adUnit not set", false);
                    continue;
                }

                if (!IsAdsRemoved(adsType))
                {
                    adsImplementation.Value.Initialize();
                }
            }

            if (Debug.isDebugBuild && _config._showMediationDebuggerAfterInit)
            {
                StartShowDebuggerRoutine().Forget();
            }

            IsInitialized = true;
        }

        private void ProcessUserConsent(MaxSdkBase.SdkConfiguration configuration)
        {
#if UNITY_EDITOR

            if (_config.testPrivacyConsent)
            {
                OnShowGDPR();
                return;
            }
#endif

#if UNITY_IOS
            //usage https://alanyeats.com/post/unityapptrackingtransparencypopup/
            // check with iOS to see if the user has accepted or declined tracking
            // var status = Unity.Advertisement.IosSupport.ATTrackingStatusBinding.GetAuthorizationTrackingStatus();
            //
            // if (status == Unity.Advertisement.IosSupport.ATTrackingStatusBinding.AuthorizationTrackingStatus
            //         .NOT_DETERMINED)
            // {
            //     OnShowGDPR();
            //     // var contextScreen = Instantiate(contextScreenPrefab).GetComponent<ContextScreenView>();
            //
            //     // after the Continue button is pressed, and the tracking request
            //     // has been sent, automatically destroy the popup to conserve memory
            //     //  contextScreen.sentTrackingAuthorizationRequest += () => Destroy(contextScreen.gameObject);
            // }

            //check automatic gdpr on ios
            var isAutomaticConsent = MaxSdkUtils.CompareVersions(UnityEngine.iOS.Device.systemVersion, "14.5") !=
                                     MaxSdkUtils.VersionComparisonResult.Lesser;

            if (!isAutomaticConsent && configuration.ConsentDialogState != MaxSdkBase.ConsentDialogState.DoesNotApply)
            {
                OnShowGDPR();
            }
            else if (isAutomaticConsent)
            {
                //automatic show native att gdpr
                StartWaitForNativeGDPRCallbackAsync(configuration).Forget();
            }
            else
            {
                Activate();
            }

#elif UNITY_ANDROID
            OnShowGDPR();
#endif
        }

#if UNITY_IOS
        private async UniTaskVoid StartWaitForNativeGDPRCallbackAsync(MaxSdkBase.SdkConfiguration configuration)
        {
            //check for native gdpr open
            while (configuration.AppTrackingStatus != MaxSdkBase.AppTrackingStatus.Authorized &&
                   configuration.AppTrackingStatus != MaxSdkBase.AppTrackingStatus.Denied)
            {
                await UniTask.Yield();
            }

            await UniTask.Delay(TimeSpan.FromSeconds(1));

            SetUserConsent(configuration.AppTrackingStatus == MaxSdkBase.AppTrackingStatus.Authorized);
        }
#endif

        private void OnShowGDPR()
        {
            Debug.Log("GameRoot: Show GDPR Popup");
            var gdprPopup = _popupsManager.GetOrCreatePopup<GdprPopup>();

            if (gdprPopup == null)
            {
                throw new Exception("MaxSDK GdprPopup not exist, create in PopupsManagerConfig");
            }

            gdprPopup.OnClosed += OnPopupClosed;
            gdprPopup.Open(1000);
        }

        private void OnPopupClosed(IGamePopup popup)
        {
            popup.OnClosed -= OnPopupClosed;
            SetUserConsent(true);
        }

        private async UniTaskVoid StartShowDebuggerRoutine()
        {
            await UniTask.DelayFrame(100);
            MaxSdk.ShowMediationDebugger();
        }

        // summary
        /// <summary>
        /// </summary>
        /// <param name="adsType"></param>
        /// <param name="addUnitId"></param>
        /// <param name="adInfo"></param>
        internal void OnAdRevenuePaidEvent(AdsType adsType, string addUnitId, MaxSdkBase.AdInfo adInfo)
        {
            var revenue = MaxSdk.GetAdInfo(addUnitId).Revenue;
            if (revenue < 0.0)
            {
                return;
            }


            if (_analytics == null) return;
            var tracker = _analytics.CreateTracker(_config._adsImpressionKey.EventName);

            if (tracker == null) return;

            var countryCode =
                MaxSdk.GetSdkConfiguration()
                    .CountryCode; // "US" for the United States, etc - Note: Do not confuse this with currency code which is "USD" in most cases!
            var adImpressionData = new AdsImpressionAnalyticData(adsType.ToString(),
                AdsShowResult.ShowComplete.ToString(), adInfo.Placement)
            {
                AdUnit = adInfo.AdUnitIdentifier,
                NetworkName = adInfo.NetworkName,
                Revenue = revenue,
                CountryCode = countryCode,
                DspName = adInfo.DspName,
                AdProvider = adInfo.WaterfallInfo.Name
            };

            tracker.TrackAdsImpression(adImpressionData, _config._adsImpressionKey.Filter);

            Log("[GET REVENUE] revenue: " + revenue + " | countryCode: " + countryCode +
                " | networkName: " + adImpressionData.NetworkName + " | adUnitIdentifier: " + adImpressionData.AdUnit +
                " | placement: " +
                adImpressionData.Placement);
        }

        public override bool IsAdsReadyToShow(AdsType adsType)
        {
            if (!PreCondition(_config._adIdsCollection.GetAdUnitId(adsType), adsType)) return false;
            return base.IsAdsReadyToShow(adsType);
        }

        public override void ShowAds(AdsType adsType, Action<AdsShowResult> callback = null, string placement = "")
        {
            var adUnit = _config._adIdsCollection.GetAdUnitId(adsType);

            if (PreCondition(adUnit, adsType))
            {
                var adsImplementation = _adsImplementations[adsType];

                adsImplementation.ShowAds(result => { callback?.Invoke(result); });
            }
            else
            {
                callback?.Invoke(AdsShowResult.NotReady);
                AdShowed(adsType, false);
            }
        }

        public void ShowMediationDebugger()
        {
            MaxSdk.ShowMediationDebugger();
        }
    }
}
#endif