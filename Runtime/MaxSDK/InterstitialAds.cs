﻿#if MAX_SDK
using System;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using Gameready.Utils;
using UnityEngine;

namespace GameReady.Ads.MAXSDK
{
    public class InterstitialAds : IAdsImplementation
    {
        private readonly MaxSDKMediation _maxSDKMediation;

        private bool _firstLaunch;

        private bool _clicked;

        private string _placement;

        private Action<AdsShowResult> _callback;

        private readonly string _adUnitId;

        //  private AnalyticsEventsTracker _analyticsEvents;

        public InterstitialAds(MaxSDKMediation maxSDKMediation)
        {
            _maxSDKMediation = maxSDKMediation;
            _adUnitId = _maxSDKMediation._config._adIdsCollection.InterstitialId;
        }

        public void Initialize()
        {
            //   _analyticsEvents = _maxSDKMediation._analyticsManager?.CreateTracker();

            // Attach callbacks
            MaxSdkCallbacks.Interstitial.OnAdClickedEvent += OnInterstitialClickedEvent;
            MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
            MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialFailedEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += InterstitialFailedToDisplayEvent;
            MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialDismissedEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent += OnInterstitialDisplayedEvent;
            if (_maxSDKMediation._config._sendPaidRevenueAnalytics)
                MaxSdkCallbacks.Interstitial.OnAdRevenuePaidEvent += OnAdRevenuePaidEventHandler;

            // Load the first interstitial
            LoadInterstitialAsync().Forget();

            if (_maxSDKMediation._config._startInterstitialAfterInterstitialTimerOnInit)
            {
                // Starts blocking timer so we don't see first ad in less than timer value
                _maxSDKMediation._interstitialBlockedAfterInterstitial = true;
                _maxSDKMediation.BlockInterstitialAfterInterstitial().Forget();
            }

            IsInitialized = true;
        }

        private void OnAdRevenuePaidEventHandler(string adUnit, MaxSdkBase.AdInfo adInfo)
        {
            _maxSDKMediation.OnAdRevenuePaidEvent(AdsType.Interstitial, adUnit, adInfo);
        }

        public bool IsInitialized { get; private set; }

        public void ShowAds(Action<AdsShowResult> callback, string placement = "")
        {
            _callback = callback;
            _placement = placement;

            if (MaxSdk.IsInterstitialReady(_adUnitId))
            {
                _firstLaunch = true;

                MaxSdk.ShowInterstitial(_adUnitId, _placement);

                _maxSDKMediation.CurrentShowState = AdsShowState.StartShowInterstitial;

                NetworkCheck.CheckConnection();

                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
                //     .SetParamValue("type", "interstitial")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "success")
                //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
                //
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsStarted)
                //     .SetParamValue("type", "interstitial")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "started")
                //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }
            else
            {
                if (_firstLaunch)
                {
                    // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
                    //     .SetParamValue("type", "interstitial")
                    //     .SetParamValue("placement", _placement)
                    //     .SetParamValue("result", "not_ready")
                    //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
                    //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
                }

                _callback?.Invoke(AdsShowResult.NotReady);
                _maxSDKMediation.AdShowed(AdsType.Interstitial, false);
            }
        }

        public void Preload()
        {
            if (!IsInitialized) return;
            MaxSdk.LoadInterstitial(_adUnitId);
        }

        public bool IsLoaded()
        {
            return IsInitialized && MaxSdk.IsInterstitialReady(_adUnitId);
        }

        public void Dispose()
        {
            IsInitialized = false;
            MaxSdkCallbacks.Interstitial.OnAdClickedEvent -= OnInterstitialClickedEvent;
            MaxSdkCallbacks.Interstitial.OnAdLoadedEvent -= OnInterstitialLoadedEvent;
            MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent -= OnInterstitialFailedEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent -= InterstitialFailedToDisplayEvent;
            MaxSdkCallbacks.Interstitial.OnAdHiddenEvent -= OnInterstitialDismissedEvent;
            MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent -= OnInterstitialDisplayedEvent;
            MaxSdkCallbacks.Interstitial.OnAdRevenuePaidEvent -= OnAdRevenuePaidEventHandler;
        }

        private void OnInterstitialDisplayedEvent(string adUnitId, MaxSdkBase.AdInfo arg2)
        {
            _callback?.Invoke(AdsShowResult.ShowComplete);
            _maxSDKMediation.AdShowed(AdsType.Interstitial, !Application.isEditor);
            _maxSDKMediation._interstitialBlockedAfterInterstitial = true;
            _maxSDKMediation.BlockInterstitialAfterInterstitial().Forget();
        }


        private void OnInterstitialClickedEvent(string arg1, MaxSdkBase.AdInfo arg2)
        {
            _clicked = true;
            _callback?.Invoke(AdsShowResult.Clicked);
            _maxSDKMediation.AdClicked(AdsType.Interstitial);
        }

        private async UniTaskVoid LoadInterstitialAsync(float delay = 0)
        {
            if (delay >= 0)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(delay));
            }

            MaxSdk.LoadInterstitial(_adUnitId);
        }

        private void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
            _maxSDKMediation.Log($"Interstitial loaded {adInfo.Placement}");

            // Reset retry attempt
            _maxSDKMediation._interstitialRetryAttempt = 0;
            _maxSDKMediation.AdLoaded(AdsType.Interstitial, true);
        }

        private void OnInterstitialFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
        {
            if (errorInfo.Code == MaxSdkBase.ErrorCode.NetworkError || errorInfo.Code == MaxSdkBase.ErrorCode.NoNetwork)
            {
                NetworkCheck.CheckConnection();
            }

            // Interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
            _maxSDKMediation._interstitialRetryAttempt++;

            float retryDelay = Mathf.Pow(2, _maxSDKMediation._interstitialRetryAttempt);

            _maxSDKMediation._interstitialRetryAttempt = Mathf.Clamp(_maxSDKMediation._interstitialRetryAttempt, 1, 6);

            _maxSDKMediation.AdLoaded(AdsType.Interstitial, false);

            _maxSDKMediation.Log("Interstitial failed to load with error code: " + errorInfo.Code, true);

            _maxSDKMediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            LoadInterstitialAsync(retryDelay).Forget();
        }

        private void InterstitialFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo,
            MaxSdkBase.AdInfo adInfo)
        {
            if (errorInfo.Code == MaxSdkBase.ErrorCode.NetworkError || errorInfo.Code == MaxSdkBase.ErrorCode.NoNetwork)
            {
                NetworkCheck.CheckConnection();
            }

            if (MaxSdk.IsInterstitialReady(_adUnitId))
            {
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsStarted)
                //     .SetParamValue("type", "interstitial")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "cancel")
                //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }

            // Interstitial ad failed to display. We recommend loading the next ad
            _maxSDKMediation.Log("Interstitial failed to display with error code: " + errorInfo.Code, true);

            _callback?.Invoke(AdsShowResult.ShowFailed);
            _maxSDKMediation.AdShowed(AdsType.Interstitial, false);

            _maxSDKMediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            LoadInterstitialAsync().Forget();
        }

        private void OnInterstitialDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            if (_clicked)
            {
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsWatched)
                //     .SetParamValue("type", "interstitial")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "watched")
                //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }
            else
            {
                // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsWatched)
                //     .SetParamValue("type", "interstitial")
                //     .SetParamValue("placement", _placement)
                //     .SetParamValue("result", "clicked")
                //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
                //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            }

            _clicked = false;

            _callback?.Invoke(AdsShowResult.Closed);
            // Interstitial ad is hidden. Pre-load the next ad
            _maxSDKMediation.AdClosed(AdsType.Interstitial);

            _maxSDKMediation.CurrentShowState = AdsShowState.ReadyForShowAds;

            LoadInterstitialAsync().Forget();
        }
    }
}
#endif