using GameReady.Ads.Data;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.Ads.MAXSDK
{
    public class MaxSDKConfig : BaseAdsManagerConfig
    {
        [SerializeField] internal AdIdsCollection _adIdsCollection;

        [SerializeField] private string _maxSDKKey;

        [SerializeField] private bool _usePrivacyConsent;

        [Tooltip("Only in dev builds")] [SerializeField]
        internal bool _showMediationDebuggerAfterInit;

        [SerializeField] internal bool _useBanner;

#if MAX_SDK
        [SerializeField] internal MaxSdkBase.BannerPosition _bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
#endif
        [SerializeField] internal Color _bannerBackColor = Color.white;

        [SerializeField] internal bool _showBannerAfterInit;

        [Tooltip("To help ensure compliance with COPPA, GDPR, other age-related requirements, " +
                 "and the Apple App Store and Google Play Store policies, you must indicate whether " +
                 "a user falls within an age-restricted category (i.e., under the age of 16). " +
                 "If you know that the user falls within an age-restricted category " +
                 "(i.e., under the age of 16), you must set the age-restricted user flag to true")]
        [SerializeField]
        internal bool _isAgeRestricted;

#if UNITY_EDITOR

        [Header("EDITOR ONLY")] public bool testPrivacyConsent;


#endif

        public string MaxSDKKey => _maxSDKKey;

        public bool UsePrivacyConsent => _usePrivacyConsent;

        public override void RegisterService(IContainerBuilder builder)
        {
#if MAX_SDK
            builder.RegisterEntryPoint<MaxSDKMediation>().As<IAdsManager>().WithParameter(this);
#endif
        }
    }
}