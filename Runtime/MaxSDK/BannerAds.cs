﻿#if MAX_SDK
using System;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using GameReady.Analytics.Core;

namespace GameReady.Ads.MAXSDK
{
    public class BannerAds : IAdsImplementation
    {
        private readonly MaxSDKMediation _maxSDKMediation;
        private bool _bannerActivated;
        private readonly string _adUnitId;

        private Action<AdsShowResult> _callback;
        private string _placement;

     //   private AnalyticsEventsTracker _analyticsEvents;

        public BannerAds(MaxSDKMediation maxSDKMediation)
        {
            _maxSDKMediation = maxSDKMediation;
        }

        public void Initialize()
        {
            if (!_maxSDKMediation._config._useBanner) return;

           // _analyticsEvents = _maxSDKMediation._analytics.CreateTracker();
            // Banners are automatically sized to 320x50 on phones and 728x90 on tablets
            // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments
            MaxSdk.CreateBanner(_adUnitId, _maxSDKMediation._config._bannerPosition);

            // Set background or background color for banners to be fully functional
            MaxSdk.SetBannerBackgroundColor(_adUnitId, _maxSDKMediation._config._bannerBackColor);

            MaxSdkCallbacks.Banner.OnAdLoadedEvent += OnBannerAdLoadedEvent;
            MaxSdkCallbacks.Banner.OnAdLoadFailedEvent += OnBannerAdLoadedFailedEvent;
            MaxSdkCallbacks.Banner.OnAdClickedEvent += OnBannerClickedEvent;

            OnBannerAdLoadedEvent(_adUnitId, null);

            if (_maxSDKMediation._config._showBannerAfterInit)
            {
                ShowAds(null);
            }

            IsInitialized = true;
        }

        private void OnBannerClickedEvent(string adUnit, MaxSdkBase.AdInfo info)
        {
            // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsStarted)
            //     .SetParamValue("type", "banner")
            //     .SetParamValue("result", "clicked")
            //     .SetParamValue("placement", _placement)
            //     .SetParamValue("connection",NetworkCheck.LastConnectionStatus.ToString().ToLower())
            //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            
            _callback?.Invoke(AdsShowResult.Clicked);
            _maxSDKMediation.AdClicked(AdsType.Banner);
        }

        private void OnBannerAdLoadedFailedEvent(string adUnit, MaxSdkBase.ErrorInfo errorInfo)
        {
            // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
            //     .SetParamValue("type", "banner")
            //     .SetParamValue("result", "not_ready")
            //     .SetParamValue("placement", _placement)
            //     .SetParamValue("connection",NetworkCheck.LastConnectionStatus.ToString().ToLower())
            //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);
            _bannerActivated = false;
            _maxSDKMediation.Log($"banner not loaded, error:{errorInfo.Message}");
            _maxSDKMediation.AdLoaded(AdsType.Banner, false);
            _callback?.Invoke(AdsShowResult.ShowFailed);
        }

        private void OnBannerAdLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
        {
            // _analyticsEvents?.SetEventKey(DefaultAnalyticKeys.AdsReady)
            //     .SetParamValue("type", "banner")
            //     .SetParamValue("result", "success")
            //     .SetParamValue("placement", _placement)
            //     .SetParamValue("connection", NetworkCheck.LastConnectionStatus.ToString().ToLower())
            //     .TrackEvents(_maxSDKMediation._config.AnalyticTrackFilter);

            IsInitialized = true;
            _maxSDKMediation.AdLoaded(AdsType.Banner, true);
            _callback?.Invoke(AdsShowResult.ShowComplete);
        }


        public bool IsInitialized { get; private set; }

        public void ShowAds(Action<AdsShowResult> callback, string placement = "")
        {
            if (_bannerActivated)
            {
                _maxSDKMediation.Log("banner already showing");
                return;
            }

            _placement = placement;
            _callback = callback;

            if (IsInitialized)
            {
                MaxSdk.ShowBanner(_adUnitId);
            }
            else
            {
                StartWaitForInitAsync().Forget();
            }

            _bannerActivated = true;
        }

        private async UniTaskVoid StartWaitForInitAsync()
        {
            await UniTask.WaitUntil(() => IsInitialized);
            MaxSdk.ShowBanner(_adUnitId);
        }

        public void Preload()
        {
            MaxSdk.LoadBanner(_adUnitId);
        }

        public bool IsLoaded()
        {
            return IsInitialized && _bannerActivated;
        }

        public void Dispose()
        {
            if (!IsInitialized) return;
            MaxSdkCallbacks.Banner.OnAdLoadedEvent -= OnBannerAdLoadedEvent;
            MaxSdkCallbacks.Banner.OnAdLoadFailedEvent -= OnBannerAdLoadedFailedEvent;
            MaxSdkCallbacks.Banner.OnAdClickedEvent -= OnBannerClickedEvent;
            MaxSdk.DestroyBanner(_adUnitId);

            IsInitialized = false;
        }
    }
}
#endif