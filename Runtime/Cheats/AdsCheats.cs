using GameReady.Ads;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
#if CHEATS
using Gameready.Cheats;
#endif
using UnityEngine;
using UnityEngine.UIElements;
using VContainer;

namespace Gameready.Ads.Cheats
{
#if CHEATS
    public class AdsCheats : BaseUIToolkitCheat
    {
        private VisualElement _mainContainer;
        private EnumField _adsTypeSelected;

        private Button _showButton;
        private Button _preloadButton;
        private Button _debuggerButton;

        private IAdsManager _adsManager;


        protected override VisualElement BuildInternal()
        {
            _adsManager = Resolver.Resolve<IAdsManager>();

            if (_adsManager == null)
            {
                return null;
            }

            if (!_adsManager.IsInitialized)
            {
                _mainContainer.SetEnabled(false);
                _adsManager.OnSDKInitialized += OnSdkInitializedHandler;
            }


            _mainContainer = _template.Instantiate();

            _adsTypeSelected = _mainContainer.Q<EnumField>("ads-type");
            // _adsTypeSelected.RegisterValueChangedCallback<AdsType>()

            _showButton = _mainContainer.Q<Button>("show-ads");
            _preloadButton = _mainContainer.Q<Button>("preload-ads");
            _debuggerButton = _mainContainer.Q<Button>("ads-debugger");

            _showButton.clicked += OnShowSelectedAds;
            _preloadButton.clicked += OnPreloadSelectedAds;
            _debuggerButton.clicked += OnShowMediationDebugger;


            return _mainContainer;
        }

        private void OnSdkInitializedHandler()
        {
            _mainContainer.SetEnabled(_adsManager.IsInitialized);
        }

        private void OnPreloadSelectedAds()
        {
            _adsManager.PreloadAds((AdsType)_adsTypeSelected.value);
        }

        private void OnShowSelectedAds()
        {
            _adsManager.ShowAds((AdsType)_adsTypeSelected.value, null, "cheats_panel");
        }

        private void OnShowMediationDebugger()
        {
            (_adsManager as IMediationDebuggerSupport)?.ShowMediationDebugger();
            GameCheatsPanel.Instance.Hide();
        }

        public override void Dispose()
        {
            base.Dispose();
            if (_showButton != null)
                _showButton.clicked -= OnShowSelectedAds;
            if (_preloadButton != null)
                _preloadButton.clicked -= OnPreloadSelectedAds;
            if (_debuggerButton != null)
                _debuggerButton.clicked -= OnShowMediationDebugger;

            _mainContainer?.RemoveFromHierarchy();
        }
    }
#endif
}