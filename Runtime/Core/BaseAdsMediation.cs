﻿using System;
using System.Collections.ObjectModel;
using Cysharp.Threading.Tasks;
using GameReady.Ads.Data;
using GameReady.Analytics;
using GameReady.Analytics.Data;
using UnityEngine;

namespace GameReady.Ads.Core
{
    public abstract class BaseAdsMediation : IAdsManager, IDisposable
    {
        private readonly BaseAdsManagerConfig _config;
        
        protected const string AdsRemovedPrefKey = "ads_removed";

        public event Action<AdsType, bool> OnAdsLoaded;
        public event Action<AdsType, bool> OnAdsShowComplete;

        public event Action<AdsType> OnAdsClosed;
        public event Action<AdsType> OnAdsClicked;

        public event Action OnSDKInitialized;

        public event Action OnAdsRemoved;

        public event Action OnNoAdsActivated;

        internal int _interstitialRetryAttempt;
        internal AdsShowState CurrentShowState;

        internal int _rewardedLoadRetryAttempt;
        internal bool _interstitialBlockedAfterInterstitial;
        internal bool _interstitialBlockedAfterRewarded;
        internal bool _isDisposed;

        protected ReadOnlyDictionary<AdsType, IAdsImplementation> _adsImplementations;
        
        private bool _isInitialized;


        protected BaseAdsMediation(BaseAdsManagerConfig config)
        {
            _config = config;
        }

        public bool IsInitialized
        {
            get => _isInitialized;
            protected set => _isInitialized = value;
        }
        
        internal void AdLoaded(AdsType type, bool result)
        {
            OnAdsLoaded?.Invoke(type, result);
        }

        internal void AdShowed(AdsType type, bool result)
        {
            OnAdsShowComplete?.Invoke(type, result);
        }

        internal void AdClosed(AdsType type)
        {
            OnAdsClosed?.Invoke(type);
        }

        internal void AdClicked(AdsType type)
        {
            OnAdsClicked?.Invoke(type);
        }


        public virtual bool PreloadAds(AdsType adsType)
        {
            if (!IsAdsReadyToShow(adsType) && CurrentShowState == AdsShowState.ReadyForShowAds)
            {
                _adsImplementations[adsType].Preload();
                return true;
            }

            return false;
        }

        public abstract void ShowAds(AdsType adsType, Action<AdsShowResult> callback = null, string placement = "");

        public virtual void RemoveAds(AdsType adsType)
        {
            if (IsAdsRemoved(adsType)) return;
            PlayerPrefs.SetInt($"{AdsRemovedPrefKey}_{adsType}", 1);

            _adsImplementations[adsType].Dispose();

            Log($"Ads-{adsType} removed!");
            OnAdsRemoved?.Invoke();
        }

        public virtual void RemoveAds()
        {
            if (!IsAdsRemoved())
            {
                PlayerPrefs.SetInt($"{AdsRemovedPrefKey}_{AdsType.Banner}", 1);
                PlayerPrefs.SetInt($"{AdsRemovedPrefKey}_{AdsType.Interstitial}", 1);

                _adsImplementations[AdsType.Interstitial].Dispose();
                _adsImplementations[AdsType.Banner].Dispose();

                Log("Ads removed!");

                OnAdsRemoved?.Invoke();
                OnNoAdsActivated?.Invoke();
            }
        }

        protected void SdkInitialized()
        {
            OnSDKInitialized?.Invoke();
        }

        public virtual bool IsAdsRemoved()
        {
            return PlayerPrefs.GetInt($"{AdsRemovedPrefKey}_{AdsType.Banner}") > 0 &&
                   PlayerPrefs.GetInt($"{AdsRemovedPrefKey}_{AdsType.Interstitial}") > 0;
        }

        public virtual bool IsAdsRemoved(AdsType adsType)
        {
            return PlayerPrefs.GetInt($"{AdsRemovedPrefKey}_{adsType}") > 0;
        }

        public virtual bool IsAdsReadyToShow(AdsType adsType)
        {
            return _adsImplementations[adsType].IsLoaded();
        }

        internal async UniTaskVoid BlockInterstitialAfterInterstitial()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(_config._ignoreInterstitialAfterInterstitial));
            _interstitialBlockedAfterInterstitial = false;
        }

        internal async UniTaskVoid BlockInterstitialAfterRewards()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(_config._ignoreInterstitialAfterRewarded));
            _interstitialBlockedAfterRewarded = false;
        }


        protected virtual bool MainPrecondition(string adUnit, AdsType adsType)
        {
            if (IsAdsRemoved(adsType))
            {
                Log($"ads removed for {adsType}");
                return false;
            }

            if (!_isInitialized)
            {
                Log("Initialize first", true);
                return false;
            }

            if (string.IsNullOrEmpty(adUnit))
            {
                Log($"{adsType} ads Unit not set", true);
                return false;
            }

            if (!_adsImplementations[adsType].IsInitialized)
            {
                Log($"{adsType} not Initialized", true);
                return false;
            }

            return true;
        }

        protected bool PreCondition(string adUnit, AdsType adsType)
        {
            if (!MainPrecondition(adUnit, adsType))
            {
                return false;
            }

            if (adsType == AdsType.Interstitial)
            {
                if (_interstitialBlockedAfterRewarded)
                {
                    Log($"{adsType} blocked after Rewarded for {_config._ignoreInterstitialAfterRewarded} sec.");
                    return false;
                }

                if (_interstitialBlockedAfterInterstitial)
                {
                    Log($"{adsType} blocked after {adsType} for {_config._ignoreInterstitialAfterInterstitial} sec.");
                    return false;
                }
            }

            if (adsType != AdsType.Banner && CurrentShowState != AdsShowState.ReadyForShowAds)
            {
                Log($"can't start {adsType} {CurrentShowState} now");
                return false;
            }

            return true;
        }


        internal void Log(string log, bool error = false)
        {
            if (error)
            {
                Debug.LogError($"AdsManager: {log}");
            }
            else
            {
                Debug.Log($"AdsManager: {log}");
            }
        }
        
        public virtual void Dispose()
        {
#if UNITY_EDITOR
            _isInitialized = false;
#endif
        }
    }
}