﻿using System;
using GameReady.Ads.Data;

namespace GameReady.Ads.Core
{
    public interface IAdsImplementation:IDisposable
    {
        void Initialize();

        bool IsInitialized { get; }


        void ShowAds(Action<AdsShowResult> callback,string placement = "");

        void Preload();

        bool IsLoaded();
    }
}