﻿namespace GameReady.Ads.Core
{
    public interface IMediationDebuggerSupport
    {
        void ShowMediationDebugger();
    }
}