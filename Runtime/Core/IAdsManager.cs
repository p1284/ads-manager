﻿using System;
using GameReady.Ads.Data;

namespace GameReady.Ads
{
    public interface IAdsManager
    {
        /// <summary>
        /// Raised when any ads type removed
        /// </summary>
        event Action OnAdsRemoved;

        /// <summary>
        /// Raised only when NoAds activated - by RemoveAds()
        /// </summary>
        event Action OnNoAdsActivated;

        event Action<AdsType, bool> OnAdsLoaded;

        event Action<AdsType, bool> OnAdsShowComplete;

        event Action<AdsType> OnAdsClicked;

        event Action<AdsType> OnAdsClosed;

        bool IsInitialized { get; }

        event Action OnSDKInitialized;

        bool PreloadAds(AdsType adsType);

        void ShowAds(AdsType adsType, Action<AdsShowResult> callback = null, string placement = "");

        void RemoveAds(AdsType adsType);

        /// <summary>
        /// Remove Banner,Interstitial only
        /// </summary>
        void RemoveAds();

        bool IsAdsRemoved();

        bool IsAdsRemoved(AdsType adsType);

        bool IsAdsReadyToShow(AdsType adsType);
    }
}