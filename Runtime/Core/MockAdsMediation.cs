﻿using System;
using GameReady.Ads.Core;
using GameReady.Ads.Data;
using UnityEngine;
using VContainer.Unity;


namespace GameReady.Ads
{
    public class MockAdsMediation : BaseAdsMediation,IInitializable
    {
        private bool _adsRemoved;

        private readonly MockAdsManagerConfig _config;

        public MockAdsMediation(MockAdsManagerConfig config) : base(config)
        {
            _config = config;
        }

        public override bool PreloadAds(AdsType adsType)
        {
            Debug.Log($"MockAdsManager preload ads:{adsType}");
            return true;
        }

        public override void ShowAds(AdsType adsType, Action<AdsShowResult> callback = null, string placement = "")
        {
            callback?.Invoke(AdsShowResult.ShowComplete);
            AdShowed(adsType, true);
            Log($"ads showed");
        }

        public override void RemoveAds(AdsType adsType)
        {
            _adsRemoved = true;
            Log($"ads:{adsType} removed");
        }

        public override void RemoveAds()
        {
            _adsRemoved = true;
            Log($"ads removed");
        }

        public override bool IsAdsRemoved()
        {
            return _adsRemoved;
        }

        public override bool IsAdsRemoved(AdsType adsType)
        {
            return _adsRemoved;
        }

        public override bool IsAdsReadyToShow(AdsType adsType)
        {
            return true;
        }


        public void Initialize()
        {
            if (IsInitialized) return;
            Debug.Log($"MockAdsManager initialized");
            IsInitialized = true;
            SdkInitialized();
        }
    }
}